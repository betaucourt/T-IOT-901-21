#include "Http.h"

String HTTP_GET(String url, headers httpHeaders, bool useHTTP10) {
  Serial.println("HTTP_GET: " + url);
  String resp;
  if (WiFi.status() == WL_CONNECTED) { //Check the current connection status
    HTTPClient http;
    http.useHTTP10(useHTTP10);
    http.begin(url);
    for (auto it = httpHeaders.begin(); it != httpHeaders.end(); it++)
    {
      http.addHeader(it->first.c_str(), it->second.c_str());
    }
    int httpCode = http.GET();                                                  //Make the request
    if (httpCode > 0) {
        resp = http.getString();
    } else {
      Serial.println("Error on HTTP_GET request. Code: " + httpCode);
    }
    http.end();
  }
  return resp;
}

String HTTP_POST(String url, String payload, headers httpHeaders, bool useHTTP10) {
  Serial.println("HTTP_POST: " + url);
  String resp;
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.useHTTP10(useHTTP10);
    http.begin(url);
    for (auto it = httpHeaders.begin(); it != httpHeaders.end(); it++)
    {
      http.addHeader(it->first.c_str(), it->second.c_str());
    }
    int httpCode = http.POST(payload);
    if (httpCode > 0) {
        resp = http.getString();
    } else {
      Serial.println("Error on HTTP_POST request. Code: " + httpCode);
    }
    http.end();
  }
  return resp;
}