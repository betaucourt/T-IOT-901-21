#ifndef CONFIG_h
#define CONFIG_h

// Network
#define WIFI_SSID "OnePlus 6"
#define WIFI_PASSWORD "clemoneplus"
#define SERVER_URL "http://13.37.217.24"

// Dolibarr
#define DOLI_USER_LOGIN "admin"
#define DOLI_USER_PASSWORD "admin"

// Servo Angles
#define SERVO_LEFT 0
#define SERVO_MIDDLE 10
#define SERVO_RIGHT 20

// Warehouses
#define WAREHOUSE_LEFT 1
#define WAREHOUSE_MIDDLE 2
#define WAREHOUSE_RIGHT 3

#endif