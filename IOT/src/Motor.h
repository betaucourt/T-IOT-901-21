#ifndef MOTOR_h
#define MOTOR_h

#include <Wire.h>
#include <M5Stack.h>
#include "GoPlus2.hpp"

#define STEPMOTOR_I2C_ADDR 0x70

#define STEPBYSTEP_RELATIVE_MODE "G91"
#define STEPBYSTEP_MOVE_MODE "G1"
#define STEPBYSTEP_VMAX "F100"
#define STEPBYSTEP_VSTOP "F0"
#define STEPBYSTEP_FORWARD_X "X10"
#define STEPBYSTEP_FORWARD_Y "Y10"
#define STEPBYSTEP_FORWARD_Z "Z10"

class Motor {
  public:
    Motor();

    void begin();

    //Servo
    void servo_setAngle(uint8_t addr, uint8_t angle);

    //Motor
    void stepByStep_setMode(char *mode);
    void StepByStep_loop(char *gcode, char *distance, char *speed);

  private:
    void SendCommand(byte addr, char *c);
    const int motorVStop = 0;
    const int motorVMax = 500;
};

#endif