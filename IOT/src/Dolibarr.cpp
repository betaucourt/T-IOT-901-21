#include "Dolibarr.h"
#include "Http.h"
#include "Config.h"
#include <ArduinoJson.h>

Dolibarr::Dolibarr() {
  this->server_url = String(SERVER_URL);
}

void Dolibarr::authenticate() {
  Serial.println("---Dolibarr Authentication---");

  String res = HTTP_GET(server_url + "/api/index.php/login?login=" + DOLI_USER_LOGIN + "&password=" + DOLI_USER_PASSWORD);
  DynamicJsonDocument resJSON(1024);
  deserializeJson(resJSON, res);
  
  if (isDebugMode()) {
    serializeJson(resJSON, Serial);
    Serial.println("");
  }

  // Parse token
  const char* token = resJSON["success"]["token"];
  setToken(token);
}

String Dolibarr::getProductById(int id) {
  Serial.println("---Dolibarr Get Product by ID: " + String(id) + "---");

  headers httpHeaders;
  httpHeaders["DOLAPIKEY"] = getToken();
  return HTTP_GET(server_url + "/api/index.php/products/" + String(id), httpHeaders);
}

String Dolibarr::getProductByRef(String ref) {
  Serial.println("---Dolibarr Get Product by Ref: " + ref + "---");

  headers httpHeaders;
  httpHeaders["DOLAPIKEY"] = getToken();
  return HTTP_GET(server_url + "/api/index.php/products/ref/" + ref, httpHeaders);
}

String Dolibarr::getWarehouses() {
  Serial.println("---Dolibarr Get All Warehouses---");

  headers httpHeaders;
  httpHeaders["DOLAPIKEY"] = getToken();
  return HTTP_GET(server_url + "/api/index.php/warehouses", httpHeaders);
}

String Dolibarr::getWarehouseById(int id) {
  Serial.println("---Dolibarr Get Warehouse by ID: " + String(id) + "---");

  headers httpHeaders;
  httpHeaders["DOLAPIKEY"] = getToken();
  return HTTP_GET(server_url + "/api/index.php/warehouses/" + String(id), httpHeaders);
}

String Dolibarr::createStockMovement(String productId, String warehouseId, int qty) {
  Serial.println("---Dolibarr Create Stock Movement---");
  Serial.println("Parameters : \n  product_id: " + productId + "\n" +
  "  warehouse_id: " + warehouseId + "\n" +
  "  qty: " + qty);

  String payload;
  StaticJsonDocument<2048> jsonPayload;
  jsonPayload["product_id"] = productId;
  jsonPayload["warehouse_id"] = warehouseId;
  jsonPayload["qty"] = qty;

  serializeJson(jsonPayload, payload);

  headers httpHeaders;
  httpHeaders["DOLAPIKEY"] = getToken();
  httpHeaders["Content-Type"] = "application/json";
  return HTTP_POST(server_url + "/api/index.php/stockmovements", payload, httpHeaders);
}

void Dolibarr::setToken(String token) {
  this->token = token;
}

String Dolibarr::getToken() {
  return this->token;
}

void Dolibarr::setDebugMode(bool debugMode) {
  this->debugMode = debugMode;
}

bool Dolibarr::isDebugMode() {
  return this->debugMode;
}