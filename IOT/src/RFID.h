#ifndef RFID_h
#define RFID_h

#include <M5Stack.h>
#include "MFRC522_I2C.h"

#define STOCK_MVT_ID 15
#define STOCK_MVT_TIMESTAMP 19
#define RFID_BUFFER_LGTH 18

class RFID {
  public:
    RFID();
    void begin();
    void showReaderDetails();
    bool isCardDetected();
    void stopReading();
    byte* getUID();
    byte getUIDSize();
    void write(int blockNum, byte writeData[]);
    void read(int blockNum, byte readBlockData[]);
    void dumpPICCType();
    void dumpUID();
    void dumpUltralightMemoryToSerial();

  private:
    MFRC522::StatusCode status;
    byte bufferLen = 18;
};

#endif