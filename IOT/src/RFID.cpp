#include "RFID.h"

MFRC522 mfrc522(0x28);

RFID::RFID() {
}

void RFID::begin() {
  Serial.print("[mfrc522].....");
  mfrc522.PCD_Init();
  delay(2000);
  showReaderDetails();
  Serial.println("OK");
}

bool RFID::isCardDetected() {
  if (mfrc522.PICC_IsNewCardPresent()) {
    if (mfrc522.PICC_ReadCardSerial()) {
      return true;
    }
    return false;
  }
  return false;
}

void RFID::stopReading() {
  mfrc522.PICC_HaltA();
}

byte *RFID::getUID() {
  return mfrc522.uid.uidByte;
}

byte RFID::getUIDSize() {
  return mfrc522.uid.size;
}

void RFID::showReaderDetails() {
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown)"));
  Serial.print(" ");
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
  }
}

void RFID::write(int blockNum, byte writeBlockData[]) 
{
  for (byte page = 0; page < 16; page += 4) {
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockNum + (page / 4), writeBlockData + page, 16);
    if (status != MFRC522::STATUS_OK)
    {
      Serial.print("Writing to Block failed: ");
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    }
    else
    {
      //Serial.println("Data was written into Block successfully");
    }
  }
}

void RFID::read(int blockNum, byte readBlockData[]) 
{
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockNum, readBlockData, &bufferLen);
  if (status != MFRC522::STATUS_OK)
  {
    Serial.print("Reading failed: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else
  {
    //Serial.println("\nBlock was read successfully");  
  }
}

void RFID::dumpPICCType() {
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = (MFRC522::PICC_Type) mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));
}

void RFID::dumpUID() {
  Serial.print(F("Card UID:"));
  for (byte i = 0; i < getUIDSize(); i++)
  {
    Serial.print(getUID()[i] < 0x10 ? " 0" : " ");
    Serial.print(getUID()[i], HEX);
  }
  Serial.print("\n");
}

void RFID::dumpUltralightMemoryToSerial() {
  mfrc522.PICC_DumpMifareUltralightToSerial();
  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
}