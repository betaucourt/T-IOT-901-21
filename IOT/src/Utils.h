#ifndef UTILS_h
#define UTILS_h

#include <M5Stack.h>
#include <ArduinoJson.h>

bool isCharArrayEmpty(const char *str);
const char *extractValueFromJsonString(String input, const char *value);

#endif