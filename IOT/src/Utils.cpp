#include "Utils.h"

bool isCharArrayEmpty(const char *str) {
  return ((str != NULL) && (str[0] == '\0'));
}

const char *extractValueFromJsonString(String input, const char *value)
{
  DynamicJsonDocument resJson(4096);
  DeserializationError deserializationError = deserializeJson(resJson, input);
  if (deserializationError)
  {
    Serial.print("deserializeJson() failed: ");
    Serial.println(deserializationError.c_str());
    return NULL;
  }
  else
  {
    const char *res = resJson[value];
    return res;
  }
}