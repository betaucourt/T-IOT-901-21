#ifndef DOLI_h
#define DOLI_h

#include <M5Stack.h>

class Dolibarr {
  public:
    Dolibarr();

    // API routes

    void authenticate();
    String getProductById(int id);
    String getProductByRef(String ref);
    String getWarehouses();
    String getWarehouseById(int id);
    String createStockMovement(String productId, String warehouseId, int qty);

    // Getters/Setters

    void setDebugMode(bool debugMode);
    bool isDebugMode();
    String getToken();
    void setToken(String token);

  private:
    String token;
    String server_url;
    bool debugMode = false;
};

#endif