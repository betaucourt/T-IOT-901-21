#include "Motor.h"

GoPlus2 goPlus;

Motor::Motor() {

}

void Motor::begin() {
    Serial.print("[goPlus].....");
    goPlus.begin();
    for (int i = 0; i < 5; i++) {
      if (!Wire.available()) {
        delay(1000);
      } else {
        break;
      }
      if (i == 4) {
        Serial.println("goPlus couldn't init i2c");
      }
    }
    Serial.println("OK");
}

void Motor::servo_setAngle(uint8_t addr, uint8_t angle) {
  // Serial.println("SERVO Write Angle " + String(addr) + " " + String(angle));
  goPlus.Servo_write_angle(addr, angle);
}

void Motor::SendCommand(byte addr, char *c) {
  Wire.beginTransmission(addr);
  while ((*c) != 0) {
    Wire.write(*c);
    c++;
  }
  Wire.write(0x0d);
  Wire.write(0x0a);
  Wire.endTransmission();
}

void Motor::stepByStep_setMode(char *mode) {
  SendCommand(STEPMOTOR_I2C_ADDR, mode);
}

void Motor::StepByStep_loop(char *gcode, char *distance, char *speed) {
  SendCommand(STEPMOTOR_I2C_ADDR, "G91");
  // String command = String(gcode) + " " + String(distance) + "Y0Z0 " + String(speed);
  String command = "G1 X100Y100Z100 F500";
  // char *ccommand = new char[command.length()];
  // strcpy(ccommand, command.c_str() );
  SendCommand(STEPMOTOR_I2C_ADDR, const_cast<char*>(command.c_str()));
  delay(650);
}