//Custom Includes
#include "Dolibarr.h"
#include "Config.h"
#include "RFID.h"
#include "Motor.h"
#include "Utils.h"
#include <M5Stack.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <Regexp.h>
#include <Queu.h>

WiFiClient wifiClient;
Dolibarr dolibarr;
RFID rfid;
Motor motor;
Queue<colis> queu(20); // declaration d'une queu qui prend une structure colis et qui a 20 slots

bool motorShouldRun = true;

bool haveOne = false;

bool connectWifi()
{
  const int wifi_max_tries = 20;
  unsigned int tries = 0;

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
    Serial.println((int)WiFi.status());
    if (tries > wifi_max_tries)
    {
      Serial.println("Couldn't connect to WiFi: timeout");
      return false;
    }
    tries++;
  }
  Serial.println("Connected to WiFi");
  return true;
}

// the setup routine runs once when M5Stack starts up
void setup()
{
  Serial.begin(115200);
  while (!Serial)
    ;
  Serial.println("[Serial].....OK");

  Serial.print("[M5].....");
  bool LCDEnable = true;
  bool SDEnable = false;
  bool SerialEnable = false;
  bool I2CEnable = true;
  M5.begin(LCDEnable, SDEnable, SerialEnable, I2CEnable);
  Serial.println("OK");
  motor.begin();

  rfid.begin();

  connectWifi();

  if (WiFi.status() == WL_CONNECTED)
  {
    dolibarr.authenticate();
    // dolibarr.getProductById(1);
  }

  motor.stepByStep_setMode(STEPBYSTEP_RELATIVE_MODE);
  motor.servo_setAngle(SERVO_NUM0, 0);

  Serial.println("[SETUP].....complete");
}

const char *extractValueFromRFIDBuffer(const char *value)
{
  String cardMemoryBuffer;
  for (int blockNum = 4; blockNum < 48; blockNum += 4)
  {
    byte readData[48];
    rfid.read(blockNum, readData);
    //Serial.println("Data for block " + String(blockNum) + ": ");
    for (int j = 0; j < 16; j++)
    {
      cardMemoryBuffer += (char)readData[j];
      //Serial.print((char)readData[j]);
    }
    //Serial.println("");
  }
  //Serial.println("Card Buffer: " + cardMemoryBuffer);

  MatchState ms;
  ms.Target(const_cast<char *>(cardMemoryBuffer.c_str()));
  char result = ms.Match("({.+})", 0);
  char buf[100];
  if (result == REGEXP_MATCHED)
  {
    String jsonBuffer = String(ms.GetCapture(buf, 0));
    jsonBuffer.replace("\n", "");
    //Serial.println("json content : " + jsonBuffer);
    DynamicJsonDocument doc(1024);
    DeserializationError deserError = deserializeJson(doc, jsonBuffer);
    if (!deserError)
    {
      // Deserialization succeeded
      const char *productRef = doc["ref"];
      //Serial.println("Ref : " + String(productRef));
      return productRef;
    }
    else
    {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(deserError.f_str());
      return NULL;
    }
  }
  else if (result == REGEXP_NOMATCH)
  {
    Serial.println("No JSON detected");
  }
  else
  {
    Serial.println("cardMemoryBuffer could not be parsed");
  }
}

void loop()
{

  if (rfid.isCardDetected())
  {
    Serial.println("Card detected");
    motorShouldRun = false;
    rfid.dumpPICCType();
    rfid.dumpUID();

    const char *productRef = extractValueFromRFIDBuffer("ref");
    String product = dolibarr.getProductByRef(productRef);
    auto product_id = extractValueFromJsonString(product, "id");
    auto warehouse_id = extractValueFromJsonString(product, "fk_default_warehouse");
    Serial.print("product_id: "); Serial.println(product_id);
    Serial.print("warehouse_id: "); Serial.println(warehouse_id);
    dolibarr.createStockMovement(String(product_id), String(warehouse_id), 1);
    haveOne = true; // un colis est present

    // DynamicJsonDocument productJSON(4096);
    // DeserializationError productDeserializationError = deserializeJson(productJSON, product);
    // if (!productDeserializationError)
    // {
    //   const char *warehouse_id = productJSON["fk_default_warehouse"];
    //   Serial.print("warehouse_id: ");
    //   Serial.println(warehouse_id);
    // }

    // byte readData[18];
    // rfid.read(4, readData);
    // for (int j = 0 ; j < 16 ; j++)
    // {
    //   Serial.print((char)readData[j]);
    //   Serial.print(" ");
    // }
    // byte readData2[18];
    // rfid.read(8, readData2);
    // for (int j = 0 ; j < 16 ; j++)
    // {
    //   Serial.print((char)readData2[j]);
    //   Serial.print(" ");
    // }

    // byte mvt_id[17] = "AAAAAAAAAAAAAAAA";
    // rfid.write(STOCK_MVT_ID, mvt_id);
    // byte mvt_timestamp[17] = "BBBBBBBBBBBBBBBB";
    // rfid.write(STOCK_MVT_TIMESTAMP, mvt_timestamp);

    // Mock a request
    // delay(5000);
    // motorShouldRun = true;
    rfid.stopReading();
  }

  if (motorShouldRun)
  {
    motor.StepByStep_loop(STEPBYSTEP_RELATIVE_MODE, STEPBYSTEP_FORWARD, STEPBYSTEP_VMAX);
  }

  
  if (queu.count() == 0 && haveOne == true) // 
  {
    // ajout d'un colis dans la queu
    colis elem;
    
    elem.reference = product_id;
    elem.position = warehouse_id;

    queu.push(elem)

    switch (warehouse_id)
    {
    case 1:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    case 2:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    case 3:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    default:
    motor.servo_setAngle(SERVO_NUM0, 0);
      break;
    }
    haveOne = false;
  }
  else if (haveOne == true) {
    // ajout d'un colis dans la queu
    colis elem;
    
    elem.reference = product_id;
    elem.position = warehouse_id;

    queu.push(elem)
    haveOne = false;
  }

  if (queu.count() != 0 ) // && laser detecte un colis
  {
    queu.pop(); // enleve le colis passer
    auto elem  = queu.front(); // get le colis suivant

    switch (warehouse_id)
    {
    case 1:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    case 2:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    case 3:
      motor.servo_setAngle(SERVO_NUM0, elem.position);
      break;
    default:
    motor.servo_setAngle(SERVO_NUM0, 0);
      break;
  }

  motor.servo_setAngle(SERVO_NUM0, 0);
  motor.servo_setAngle(SERVO_NUM1, 0);
  motor.servo_setAngle(SERVO_NUM2, 0);
  motor.servo_setAngle(SERVO_NUM3, 0);
  delay(1000);
  motor.servo_setAngle(SERVO_NUM0, 15);
  motor.servo_setAngle(SERVO_NUM1, 15);
  motor.servo_setAngle(SERVO_NUM2, 15);
  motor.servo_setAngle(SERVO_NUM3, 15);
  delay(1000);
}