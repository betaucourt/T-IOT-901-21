#ifndef HTTP_h
#define HTTP_h

#include <M5Stack.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <StreamUtils.h>
#include <map>

typedef std::map<String, String> headers;

String HTTP_GET(String url, const headers = headers(), bool useHTTP10 = true);
String HTTP_POST(String url, String payload, const headers = headers(),   bool useHTTP10 = true);

#endif