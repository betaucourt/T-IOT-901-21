# T-IOT-901-msc2022
The conveyor

## Gandalf
https://gandalf.epitech.eu/course/view.php?id=137

## Trello
https://trello.com/b/YkrvJXm4

## Requirements
- Arduino IDE
- Your dolibarr credentials
- Your wifi SSID and password
- M5stackGo	
- MFRC522 RFID Unit	
- M5 GRBL	
- M5 Goplus2 
- SG90 servo
- Power supply	
- Stepper motor
- A conveyor belt



![Example](doc/conveyor.png)


## Features
- Roll the conveyor belt forward
- Scan a RFID tag and identify the parcel ID
- Get the parcel's destination warehouse ID from Dolibarr using its ID
- Create a stock movement in Dolibarr's database
- Move the servo to direct the parcel on the conveyor belt according to its destination warehouse


## How to compile your conveyor firmware
  
In order to use the firmware, please follow these steps:

- Download the iot_conveyor and required_libraries zip archives and unzip them
- Open the iot_conveyor.ino file with the Arduino IDE
- In the IDE, update the Config.h file with your own network and dolibarr variable values
- Try and verify the code by clicking the Verify button

 > If libraries are missing, you can either download them one by one or use the ones we provided in the archive file. Once the libraries are downloaded, try and compile again

 If it compiles ok, upload the firmware to your M5.

Now on to testing !